<?php

return [
    'driver'    => 'mysql',
    'host'      =>  $_ENV['HOST'],
    'database'  =>  $_ENV['DATABASE'],
    'username'  => $_ENV['USERNAME'],
    'password'  =>  $_ENV['PASSWORD'],
    'charset'   => 'utf8mb4',
    'collation' => 'utf8mb4_unicode_ci',
    'prefix'    => '',
];
