<?php

declare(strict_types=1);

namespace App\Controllers\Game;

use App\Repositories\Game\GameRepository;
use App\Repositories\Wallet\WalletRepository;
use App\Services\WalletService;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Views\PhpRenderer;
use Psr\Http\Message\ServerRequestInterface as Request;
session_cache_limiter();
session_start();

class GameController
{
    private $renderer;
    private $gameRepository;
    private $walletRepository;
    private $walletService;
    public function __construct(
        GameRepository $gameRepository,
        WalletRepository $walletRepository,
        WalletService $walletService
    ) {
        $this->renderer = new PhpRenderer(__DIR__ . '/../../View/');
        $this->gameRepository = $gameRepository;
        $this->walletRepository = $walletRepository;
        $this->walletService = $walletService;
    }

    public function play(Request $request, Response $response): Response
    {
        $data = [];
        $user = isset($_SESSION['user']) ? $_SESSION['user'] : null;
        $formData = $request->getParsedBody();
        //check validate
        $bet_amount = (float) $formData['bet_amount'] ?? null;
        $profit = (float) $formData['profit'] ?? null;

        if (!$bet_amount || !$profit) {
            $data['message'] = 'bet_amount  or profit is null';
            $response->getBody()->write(json_encode($data));

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(500);
        }
        // calculator amount
        $amount = $this->walletService->calculatorAmount($bet_amount, $profit);
        if ($user) {
            // when user play
            $wallet = $this->walletRepository->getWalletByUserId($user->id);
            $data['wallet'] = $wallet;

            if (!$wallet) {
                $data['message'] = 'You are out of money';
                $response->getBody()->write(json_encode($data));

                return $response
                    ->withHeader('Content-Type', 'application/json')
                    ->withStatus(500);
            }

            $checkBalance = $this->walletService->checkBalance($bet_amount, $wallet);
            if (!$checkBalance) {
                $data['message'] = 'You are out of money';
                $response->getBody()->write(json_encode($data));

                return $response
                    ->withHeader('Content-Type', 'application/json')
                    ->withStatus(500);
            }

            $wallet = $this->walletRepository->updateAmount($wallet, $amount);
            $data['wallet'] = $wallet;
            $data['user'] = $user;
        }
        // when play try
        $gameArray = [];
        $gameArray['bet_amount'] = $bet_amount;
        $gameArray['profit'] = $profit;
        $gameArray['user'] = $user;
        $gameArray['amount'] = $amount;
        $game = $this->gameRepository->create($gameArray);

        $data['message'] = 'play game success';
        $data['game'] = $game;
        $data['amount'] = number_format($amount, 3);
        $response->getBody()->write(json_encode($data));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    }
}
