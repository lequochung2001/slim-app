<?php
declare(strict_types=1);

namespace App\Controllers\Auth;
use App\Models\User;
use App\Repositories\User\UserRepository;
use Slim\Views\PhpRenderer;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
session_cache_limiter();
session_start();

class AuthController
{
    private $renderer;
    private $userRepository;
    public function __construct(UserRepository $userRepository)
    {
        $this->renderer = new PhpRenderer(__DIR__ . '/../../View/');
        $this->userRepository = $userRepository;
    }

    public function login(Request $request, Response $response): Response
    {
        return  $this->renderer->render($response, "login.php");
    }

    public function handleLogin(Request $request, Response $response): Response
    {
        $formData = $request->getParsedBody();
        $email = $formData['email'] ?? null  ;
        $password = $formData['password'] ?? null;
        if(!$email || !$password){
            return $response
            ->withHeader('Location', '/login')
            ->withStatus(302);
        }
        $user = $this->userRepository->getByEmail($email);
        if (password_verify($password, $user->password)) {
            $_SESSION['user'] = $user;
            return $response
                ->withHeader('Location', '/')
                ->withStatus(302);
        }
        return $response
            ->withHeader('Location', '/login')
            ->withStatus(302);
    }
}
