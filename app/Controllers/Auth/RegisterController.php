<?php

declare(strict_types=1);

namespace App\Controllers\Auth;

use App\Repositories\User\UserRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Views\PhpRenderer;
use Psr\Http\Message\ServerRequestInterface as Request;

class RegisterController
{
    private $renderer;
    private $userRepository;
    public function __construct(UserRepository $userRepository)
    {
        $this->renderer = new PhpRenderer(__DIR__ . '/../../View/');
        $this->userRepository = $userRepository;
    }

    public function register(Request $request, Response $response): Response
    {
        return  $this->renderer->render($response, "register.php");
    }

    public function handleRegister(Request $request, Response $response): Response
    {
        $formData = $request->getParsedBody();
        $data = [];
        $data['email'] = $formData['email'];

        if($this->userRepository->getByEmail($data['email'])){
            return $response
            ->withHeader('Location', '/register')
            ->withStatus(302);
        }
        $data['password'] = $formData['password'];
        $data['username'] = $formData['username'];
        $this->userRepository->create($data);
        return $response
            ->withHeader('Location', '/login')
            ->withStatus(302);
    }
}
