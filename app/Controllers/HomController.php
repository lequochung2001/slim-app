<?php

declare(strict_types=1);

namespace App\Controllers;

session_cache_limiter();
session_start();

use App\Repositories\Game\GameRepository;
use App\Repositories\Wallet\WalletRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Views\PhpRenderer;
use Psr\Http\Message\ServerRequestInterface as Request;

class HomController
{
    private $renderer;
    private $walletRepository;
    private $gameRepository;
    public function __construct(
        WalletRepository $walletRepository,
        GameRepository $gameRepository
    ) {
        $this->renderer = new PhpRenderer(__DIR__ . '/../View/');
        $this->walletRepository = $walletRepository;
        $this->gameRepository = $gameRepository;
    }

    public function index(Request $request, Response $response): Response
    {
        $user = isset($_SESSION['user']) ? $_SESSION['user'] : null;
        $amount_wallet = 0;
        if ($user) {
            $amount_wallet = $this->walletRepository->getWalletByUserId($user->id)->formatAmount;
        }
        $games = $this->gameRepository->getLatestListPaginate();
        $hashKey = $_ENV['HASH_KEY'];
        return $this->renderer->render($response, "home.php", [
            'user' => $user,
            'hashKey' => $hashKey,
            'amount_wallet' => $amount_wallet,
            'games' => $games
        ]);
    }
}
