<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js" integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous"></script>
</head>

<body>
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
            <a class="navbar-brand" href="http://localhost:8000/sign-in">Sign In</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="http://localhost:8000/">Home</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div id="intro" class="bg-image shadow-2-strong">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5 col-md-8">
                    <form method="post" action="http://localhost:8000/register" class="bg-white rounded shadow-5-strong p-5">
                        <div class="form-outline mb-4" data-mdb-input-init>
                            <input type="text" name="username" id="form1Example1" class="form-control" placeholder="Email address" />
                        </div>
                        <!-- Email input -->
                        <div class="form-outline mb-4" data-mdb-input-init>
                            <input type="email" name="email" id="form1Example1" class="form-control" placeholder="Email address" />
                        </div>

                        <!-- Password input -->
                        <div class="form-outline mb-4" data-mdb-input-init>
                            <input type="password" name="password" id="form1Example2" placeholder="Password" class="form-control" />
                        </div>

                        <!-- 2 column grid layout for inline styling -->
                        

                        <!-- Submit button -->
                        <button type="submit" class="btn btn-primary btn-block" data-mdb-ripple-init>Sign in</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>

</html>