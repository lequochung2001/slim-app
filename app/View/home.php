<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js" integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
</head>

<body>
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
            <?php if ($user) : ?>
                <a class="navbar-brand">Hello <?= $user->username ?></a>
                <a class="navbar-brand" id="amount_wallet">Amount: <?= $amount_wallet ?></a>
            <?php else : ?>
                <a class="navbar-brand" href="http://localhost:8000/login">Sign In</a>
            <?php endif; ?>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="http://localhost:8000/">Home</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container text-center mt-5">
        <h1>Random Number Game</h1>
        <div class="d-flex justify-content-center">
            <button id="startBtn" class="btn btn-primary" onclick="startGame()">Start</button>
            <div class="col-2 ms-2">
                <input type="text" value="1000" require name="bet" id="form1Example1_bet" class="form-control" placeholder="bet" />
            </div>
        </div>
        <p id="result" class="mt-3 display-4 font-weight-bold text-primary">0</p>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <table id="product_table" class="table table-bordered dt-responsive table-striped" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th width="15%">id</th>
                                <th>username</th>
                                <th>bet_amount</th>
                                <th>profit</th>
                                <th>amount</th>
                            </tr>
                        </thead>
                        <tbody id="list-history">

                            <?php foreach ($games as $game) : ?>
                                <tr>
                                    <td><?= $game->id; ?></td>
                                    <td><?= $game->user?->username ?? 'customer'; ?></td>
                                    <td><?= $game->bet_amount; ?></td>
                                    <td><?= $game->profit; ?></td>
                                    <td><?= $game->amount; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>

                    </table>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

    <script>
        function startGame() {
            // Disable the button during animation
            document.getElementById('startBtn').disabled = true;
            // Get a reference to the result paragraph
            const resultElement = document.getElementById('result');

            // Generate a random number between 0 and 2.2
            const targetNumber = Math.random() * 2.2;

            // Set the interval for changing the displayed number
            const intervalId = setInterval(() => {
                // Display a random number between 0 and 2.2
                const randomNumber = Math.random() * 2.2;
                resultElement.textContent = randomNumber.toFixed(1);
            }, 100);

            // After a random time (between 2 to 5 seconds), stop the animation
            setTimeout(() => {
                clearInterval(intervalId);
                // Display the final target number
                resultElement.textContent = targetNumber.toFixed(1);
                // Enable the button for the next round
                document.getElementById('startBtn').disabled = false;

                onGameEnd(targetNumber);
            }, Math.random() * 3000 + 2000); // Random time between 2 to 5 seconds
        }

        function onGameEnd(finalNumber) {
            console.log('Game ended with number:', finalNumber);
            data = {
                bet_amount: $("#form1Example1_bet").val(),
                profit: finalNumber,
            };
            $.ajax({
                url: 'http://localhost:8000/games/play',
                datatype: "html",
                type: "post",
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'language': 'en',
                    'content': "game_balenca_" + "<?= $hashKey ?>",
                },
                success: function(response) {
                    message = response.message;
                    if (response.wallet) {
                        $('#amount_wallet').text(response.wallet.formatAmount);
                    }
                    amount = response.amount > 0 ? " + " + response.amount : response.amount;
                    message = message + ": " + amount;
                    game = response.game
                    user = response.user;
                    toastr.success(message)
                    $('#list-history').prepend(`
                    <tr>
                        <td>${game.id} </td>
                        <td>${user?.username ?? "customer"}</td>
                        <td>${game.bet_amount}</td>
                        <td>${game.profit}</td>
                        <td>${game.amount}</td>
                    </tr>
                    `)
                },
                error: function(error) {
                    console.log("error: ", error);
                    toastr.error(text)
                },
            })
        }
    </script>
</body>

</html>