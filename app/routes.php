<?php

declare(strict_types=1);

use App\Controllers\Auth\AuthController;
use App\Controllers\Auth\RegisterController;
use App\Controllers\Game\GameController;
use App\Controllers\HomController;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->get('/', HomController::class . ":index")->setName('home');;
    $app->get('/login', AuthController::class . ":login")->setName('login');
    $app->post('/login', AuthController::class . ":handleLogin")->setName('handleLogin');

    $app->get('/register', RegisterController::class . ":register")->setName('register');
    $app->post('/register', RegisterController::class . ":handleRegister")->setName('handleRegister');

    $app->post('/games/play', GameController::class . ":play")->setName('play');
};
