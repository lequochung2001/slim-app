<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Wallet;

class WalletService
{
    public function checkBalance(float|string $amount, $wallet)
    {
        if ($wallet->amount > $amount) {
            return true;
        }
        return false;
    }

    public function calculatorAmount($bet_amount, $profit)
    {
        return ($bet_amount * $profit) - $bet_amount;
    }
}