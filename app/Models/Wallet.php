<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $table = 'wallets';
    protected $fillable = ['amount', 'user_id'];
    public $timestamps = false;

    #[\ReturnTypeWillChange]
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function getFormatAmountAttribute()
    {
        return number_format((float) $this->amount, 3);
    }
}
