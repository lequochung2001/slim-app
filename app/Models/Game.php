<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table = 'games';
    protected $fillable = ['action_id', 'bet_amount', 'amount', 'profit', 'user_id'];
    public $timestamps = false;

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
