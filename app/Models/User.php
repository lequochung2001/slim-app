<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users'; // Specify the table name
    protected $fillable = ['username', 'email']; // Specify the fillable fields
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
    }

    #[\ReturnTypeWillChange]
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}