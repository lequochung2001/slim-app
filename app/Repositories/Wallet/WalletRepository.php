<?php

declare(strict_types=1);

namespace App\Repositories\Wallet;

use App\Models\Wallet;
use App\Repositories\BaseRepository;

class WalletRepository  extends BaseRepository
{
      /**
     * Specify Model class name
     *
     * @return string
     */
    public function getModel(): string
    {
        return Wallet::class;
    }

    public function create($attributes = []): Wallet {
        $user_wallet = new $this->model;
        $user_wallet->amount = $attributes['amount'];
        $user_wallet->user_id = $attributes['user_id'];
        $user_wallet->status = 1;
        $user_wallet->save();
        return $user_wallet;
    }

    public function getWalletByUserId(int $user_id): Wallet
    {
        $model = $this->model;
        return $this->whereUserId($model, $user_id)->first();
    }

    public function whereUserId($model, string|int $user_id)
    {
        return $model->where('user_id', $user_id);
    }

    public function updateAmount($wallet, $amount): Wallet
    {
        $wallet->amount = $wallet->amount + $amount;
        $wallet->save();
        return $wallet;
    }
    
}
