<?php

declare(strict_types=1);

namespace App\Repositories\User;

use App\Models\User;
use App\Repositories\BaseRepository;
use App\Repositories\Wallet\WalletRepository;
use Slim\App;

class UserRepository  extends BaseRepository
{
    protected $walletRepository;

    public function __construct(App $app, WalletRepository $walletRepository)
    {
        $this->walletRepository = $walletRepository;
        parent::__construct($app);
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function getModel(): string
    {
        return User::class;
    }

    public function create($attributes = []): User
    {
        $user = new $this->model;
        $user->email = $attributes['email'];
        $user->password = password_hash($attributes['password'], PASSWORD_DEFAULT);
        $user->username = $attributes['username'];
        $user->save();
        $wallet_array['amount'] = 100000;
        $wallet_array['user_id'] = $user->id;
        $wallet_array['status'] = 1;
        $this->walletRepository->create($wallet_array);
        return $user;
    }

    public function getByEmail(null|string $email): User|null
    {
        $user = $this->model;
        return $this->whereEmail($user, $email)->first();
    }

    public function whereEmail(User $user, string|null $email)
    {
        return $user->where('email', $email);
    }
}
