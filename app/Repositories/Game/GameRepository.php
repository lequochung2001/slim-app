<?php

declare(strict_types=1);

namespace App\Repositories\Game;

use App\Models\Game;
use App\Repositories\BaseRepository;

class GameRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function getModel(): string
    {
        return Game::class;
    }

    public function create($attributes = []): Game
    {
        $user_id = isset($attributes['user']) ? $attributes['user']->id : null;  
        $game = new $this->model;
        $game->bet_amount = $attributes['bet_amount'];
        $game->amount = $attributes['amount'];
        $game->user_id = $user_id;
        $game->profit = $attributes['profit'];
        $game->action_id = password_hash($_ENV['HASH_KEY'], PASSWORD_DEFAULT);
        $game->save();
        return $game;
    }

    public function getLatestListPaginate($paginate = 10){
        $games = new $this->model;
        $games =  $this->withUser($games);
        $games = $this->orderbyLatest($games);
        $games = $this->getPaginate($games, $paginate);
        return $games;
    }

    public function orderbyLatest($model){
        return $model->orderBy('created_at', 'DESC');
    }

    public function getPaginate($model, $paginate = 10){
        return $model->paginate($paginate);
    }

    public function withUser($model){
        return $model->with('user');
    }
}
